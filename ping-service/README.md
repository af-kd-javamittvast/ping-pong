# Ping Service

## Docker
Package the "ping-service" into a jar file:
```
mvnw clean package
```

Build the docker image:
```
docker build -t ping-service .
```

Run the ping-service in docker:
```
docker run -p 8070:8070 -e pong_url=http://192.168.99.100:8080 -t ping-service
```
