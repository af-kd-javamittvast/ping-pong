package mittvast.pingservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class PingController {

    @Value("${pong.url}")
    private String url;
    private final RestTemplate client;

    public PingController(RestTemplate client) {
        this.client = client;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String ping(ModelMap model) {
        System.out.println(url);
        return "ping - " + client.getForObject(url, String.class);
    }

}
